package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class Incident extends ModelBase{
    private String name;
    private String code;
    @OneToOne(optional = false)
    private IncidentCategory incidentCategory;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public IncidentCategory getIncidentCategory() {
        return incidentCategory;
    }

    public void setIncidentCategory(IncidentCategory incidentCategory) {
        this.incidentCategory = incidentCategory;
    }


}
