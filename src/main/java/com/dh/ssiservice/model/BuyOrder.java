package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class BuyOrder extends ModelBase{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String code;
    @OneToMany(mappedBy = "buyOrder", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<BuyOrderEntry> buyOrderEntries = new ArrayList<>();
    public List<BuyOrderEntry> getBuyOrderEntries() {
        return buyOrderEntries;
    }

    public void setBuyOrderEntries(List<BuyOrderEntry> buyOrderEntries) {
        this.buyOrderEntries = buyOrderEntries;
    }


}
