package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class Communication extends ModelBase{
    private String type;
    private String value;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
