package com.dh.ssiservice.model;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
public class TrainingReg extends ModelBase {
    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }

    @OneToOne(optional = false)
    private Training training;
}
