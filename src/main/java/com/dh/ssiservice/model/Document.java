package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class Document extends ModelBase{
    private String type;
    private String code;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
