package com.dh.ssiservice.model;

import javax.persistence.Entity;

@Entity
public class UnitOfMeasure extends ModelBase {
    String name;
    String code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
