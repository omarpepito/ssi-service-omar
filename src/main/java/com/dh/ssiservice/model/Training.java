package com.dh.ssiservice.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Training extends ModelBase{
    private Date date;
    @OneToOne(optional = false)
    private Area area;
    @OneToOne(optional = false)
    private Position position;
    @OneToMany(mappedBy = "training", fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
    private List<TrainingReg> trainingRegs = new ArrayList<>();

    public List<TrainingReg> getTrainingRegs() {
        return trainingRegs;
    }

    public void setTrainingRegs(List<TrainingReg> trainingRegs) {
        this.trainingRegs = trainingRegs;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }
}
