package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.*;

public class InventoryCommand {
    private Long id;
    private Item item;
    private UnitOfMeasure unitOfMeasure;
    private int cant;
    private String status;

    public InventoryCommand(Inventory inventory) {
        this.setId(inventory.getId());
        this.setItem(inventory.getItem());
        this.setUnitOfMeasure(inventory.getUnitOfMeasure());
        this.setCant(inventory.getCant());
        this.setStatus(inventory.getStatus());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public UnitOfMeasure getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(UnitOfMeasure unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Inventory toDomain() {
        Inventory inventory = new Inventory();
        inventory.setItem(getItem());
        inventory.setId(getId());
        inventory.setStatus(getStatus());
        inventory.setUnitOfMeasure(getUnitOfMeasure());
        inventory.setCant(getCant());

        return inventory;
    }
}
