/**
 * (C) 2017 Agilysys NV, LLC.  All Rights Reserved.  Confidential Information of Agilysys NV, LLC.
 */
package com.dh.ssiservice.dao;


import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.Item;
import org.apache.tomcat.util.codec.binary.Base64;

public class BuyOrderCommand {

    private String name;
    private String code;
    private Long id;


    public BuyOrderCommand(BuyOrder buyOrder) {

        this.setName(buyOrder.getName());
        this.setCode(buyOrder.getCode());
        this.setId(buyOrder.getId());

    }

    public BuyOrderCommand() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BuyOrder toDomain() {
        BuyOrder buyOrder = new BuyOrder();
        buyOrder.setCode(getCode());
        buyOrder.setId(getId());
        buyOrder.setName(getName());
        return buyOrder;
    }
}
