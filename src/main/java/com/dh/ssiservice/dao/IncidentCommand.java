package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.Position;
import com.dh.ssiservice.model.Training;

import java.util.Date;

public class IncidentCommand {
    private Long id;
    private String name;
    private String code;


    public IncidentCommand(Incident incident) {
        this.setId(incident.getId());
        this.setName(incident.getName());
        this.setCode(incident.getCode());

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Incident toDomain() {
        Incident incident = new Incident();
        incident.setName(getName());
        incident.setId(getId());
        incident.setCode(getCode());

        return incident;
    }
}
