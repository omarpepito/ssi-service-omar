package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Position;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.model.TrainingReg;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TrainingCommand {
    private Long id;


    private String skill;
    private Area area;
    private Position position;
    private Date initDate;

    public TrainingCommand(Training training) {
        this.setId(training.getId());
        this.setArea(training.getArea());
        this.setInitDate(training.getDate());
        this.setPosition(training.getPosition());
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }


    public Training toDomain() {
        Training training = new Training();
        training.setDate(getInitDate());
        training.setId(getId());
        training.setArea(getArea());

        return training;
    }
}
