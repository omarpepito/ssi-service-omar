package com.dh.ssiservice.dao;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.IncidentRegistry;

import java.util.Date;

public class IncidentRegistryCommand {
    private Long id;
    private Date date;
    private Area area;
    private Incident incident;


    public IncidentRegistryCommand(IncidentRegistry incidentRegistry) {
        this.setId(incidentRegistry.getId());
        this.setDate(incidentRegistry.getDate());
        this.setArea(incidentRegistry.getArea());
        this.setIncident(incidentRegistry.getIncident());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Incident getIncident() {
        return incident;
    }

    public void setIncident(Incident incident) {
        this.incident = incident;
    }

    public IncidentRegistry toDomain() {
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setDate(getDate());
        incidentRegistry.setId(getId());
        incidentRegistry.setArea(getArea());
        incidentRegistry.setIncident(getIncident());

        return incidentRegistry;
    }
}
