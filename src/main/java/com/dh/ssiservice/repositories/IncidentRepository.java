package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Incident;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRepository extends CrudRepository<Incident, Long> {
}
