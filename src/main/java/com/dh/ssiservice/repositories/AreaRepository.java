package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.Area;
import com.dh.ssiservice.model.BuyOrder;
import org.springframework.data.repository.CrudRepository;

public interface AreaRepository extends CrudRepository<Area,Long> {
}
