package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.IncidentCategory;
import com.dh.ssiservice.model.Training;
import org.springframework.data.repository.CrudRepository;

public interface IncidentCategoryRepository extends CrudRepository<IncidentCategory, Long> {
}
