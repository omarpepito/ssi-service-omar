package com.dh.ssiservice.repositories;

import com.dh.ssiservice.model.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

public interface IncidenRegistryRepository extends CrudRepository<IncidentRegistry,Long> {
}
