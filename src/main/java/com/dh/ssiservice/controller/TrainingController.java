/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/trainings")
@Produces("application/json")
@CrossOrigin
public class TrainingController {
    private TrainingService trainingService;

    public TrainingController(TrainingService trainingService) {
        this.trainingService = trainingService;
    }

    @GET
    public Response getTrainings() {
        List<TrainingCommand> trainingCommands = new ArrayList<>();
        trainingService.findAll().forEach(training -> {
            TrainingCommand itemCommand = new TrainingCommand(training);
            trainingCommands.add(itemCommand);
        });
        return Response.ok(trainingCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Training training = trainingService.findById(id);
        return Response.ok(new TrainingCommand(training)).build();
    }
}
