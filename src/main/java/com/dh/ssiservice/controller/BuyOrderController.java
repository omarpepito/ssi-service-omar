/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.BuyOrderCommand;
import com.dh.ssiservice.dao.ItemCommand;
import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.Item;
import com.dh.ssiservice.services.BuyOrderService;
import com.dh.ssiservice.services.ItemService;
import com.dh.ssiservice.services.SubCategoryService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/buyOrders")
@Produces("application/json")
@CrossOrigin
public class BuyOrderController {
    private BuyOrderService service;

    public BuyOrderController(BuyOrderService service) {
        this.service = service;
    }

    @GET
    public Response getBuyOrders() {
        List<BuyOrderCommand> buyOrderCommands = new ArrayList<>();
        service.findAll().forEach(buyOrder -> {
            BuyOrderCommand buyOrderCommand = new BuyOrderCommand(buyOrder);
            buyOrderCommands.add(buyOrderCommand);
        });
        return Response.ok(buyOrderCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getBuyOrdersById(@PathParam("id") @NotNull Long id) {
        BuyOrder buyOrder = service.findById(id);
        return Response.ok(new BuyOrderCommand(buyOrder)).build();
    }




}