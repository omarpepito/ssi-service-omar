/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentCommand;
import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.IncidentService;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidents")
@Produces("application/json")
@CrossOrigin
public class IncidentController {
    private IncidentService incidentService;

    public IncidentController(IncidentService incidentService) {
        this.incidentService = incidentService;
    }

    @GET
    public Response getIncidents() {
        List<IncidentCommand> incidentCommands = new ArrayList<>();
        incidentService.findAll().forEach(incident -> {
            IncidentCommand incidentCommand = new IncidentCommand(incident);
            incidentCommands.add(incidentCommand);
        });
        return Response.ok(incidentCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        Incident incident = incidentService.findById(id);
        return Response.ok(new IncidentCommand(incident)).build();
    }
}
