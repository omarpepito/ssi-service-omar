/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.IncidentCommand;
import com.dh.ssiservice.dao.IncidentRegistryCommand;
import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.services.IncidentRegistryService;
import com.dh.ssiservice.services.IncidentService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidentsRegistries")
@Produces("application/json")
@CrossOrigin
public class IncidentRegistryController {
    private IncidentRegistryService incidentRegistryService;

    public IncidentRegistryController(IncidentRegistryService incidentRegistryService) {
        this.incidentRegistryService = incidentRegistryService;
    }

    @GET
    public Response getIncidentsRegistries() {
        List<IncidentRegistryCommand> incidentRegistryCommands = new ArrayList<>();
        incidentRegistryService.findAll().forEach(incidentRegistry -> {
            IncidentRegistryCommand incidentRegistryCommand = new IncidentRegistryCommand(incidentRegistry);
            incidentRegistryCommands.add(incidentRegistryCommand);
        });
        return Response.ok(incidentRegistryCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getItemsById(@PathParam("id") @NotNull Long id) {
        IncidentRegistry incidentRegistry = incidentRegistryService.findById(id);
        return Response.ok(new IncidentRegistryCommand(incidentRegistry)).build();
    }
}
