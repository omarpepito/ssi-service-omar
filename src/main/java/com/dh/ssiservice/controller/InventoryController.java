/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.controller;

import com.dh.ssiservice.dao.InventoryCommand;
import com.dh.ssiservice.dao.TrainingCommand;
import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.services.InventoryService;
import com.dh.ssiservice.services.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/inventories")
@Produces("application/json")
@CrossOrigin
public class InventoryController {
    private InventoryService inventoryService;

    public InventoryController(InventoryService inventoryService) {
        this.inventoryService = inventoryService;
    }

    @GET
    public Response getImventories() {
        List<InventoryCommand> inventoryCommands = new ArrayList<>();
        inventoryService.findAll().forEach(inventory -> {
            InventoryCommand inventoryCommand = new InventoryCommand(inventory);
            inventoryCommands.add(inventoryCommand);
        });
        return Response.ok(inventoryCommands).build();
    }

    @GET
    @Path("/{id}")
    public Response getInventoriesById(@PathParam("id") @NotNull Long id) {
        Inventory inventory = inventoryService.findById(id);
        return Response.ok(new InventoryCommand(inventory)).build();
    }
}
