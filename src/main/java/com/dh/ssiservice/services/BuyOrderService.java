/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.BuyOrder;
import com.dh.ssiservice.model.Training;

public interface BuyOrderService extends GenericService<BuyOrder> {

}
