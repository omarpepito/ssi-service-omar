/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Inventory;
import com.dh.ssiservice.model.Training;

public interface InventoryService extends GenericService<Inventory> {

}
