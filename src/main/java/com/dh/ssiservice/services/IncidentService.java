/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.Incident;
import com.dh.ssiservice.model.Training;

public interface IncidentService extends GenericService<Incident> {

}
