/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.model.Training;

public interface IncidentRegistryService extends GenericService<IncidentRegistry> {

}
