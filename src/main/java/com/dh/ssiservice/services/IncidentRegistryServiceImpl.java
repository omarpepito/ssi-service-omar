/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.IncidentRegistry;
import com.dh.ssiservice.model.Training;
import com.dh.ssiservice.repositories.IncidenRegistryRepository;
import com.dh.ssiservice.repositories.TrainingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {
    private IncidenRegistryRepository incidenRegistryRepository;

    public IncidentRegistryServiceImpl(IncidenRegistryRepository incidenRegistryRepository) {
        this.incidenRegistryRepository = incidenRegistryRepository;
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository() {
        return incidenRegistryRepository;
    }
}
