/**
 * @author: Edson A. Terceros T.
 */

package com.dh.ssiservice.services;

import com.dh.ssiservice.model.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {
}